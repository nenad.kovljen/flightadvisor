package com.nkovljen.flightadvisor.service;

import com.nkovljen.flightadvisor.model.Airport;

import java.util.List;

public interface AirportService {

    List<Airport> importAirports(String cityName);

    List<Airport> findAirportForCity(String cityName);

    void deleteAirport(Airport airport);

    void deleteAirportsForCity(String cityName);
}
