package com.nkovljen.flightadvisor.service;

import com.nkovljen.flightadvisor.model.Route;
import com.nkovljen.flightadvisor.model.RouteSummaryDto;

import javax.transaction.Transactional;
import java.util.List;

public interface RouteService {
    @Transactional
    List<Route> importRoutes(Integer airportId);

    @Transactional
    List<RouteSummaryDto> findRoutesByAirportId(String sourceCity, String destinationCity, int nrOfStops);

    @Transactional
    void deleteRoutesForAirport(Integer airportId);
}
