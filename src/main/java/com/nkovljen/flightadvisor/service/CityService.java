package com.nkovljen.flightadvisor.service;

import com.nkovljen.flightadvisor.dto.CityDto;

import java.util.List;

public interface CityService {

    CityDto save(CityDto city);

    List<CityDto> findAll(Integer nrOfComments);

    CityDto getCity(Long cityId, Integer nrOfComments);

    CityDto addComment(Long cityId, String commentText);

    CityDto updateComment(Long cityId, String commentText, Long commentId);

    CityDto removeComment(Long cityId, Long commentId);

    void delete(Long cityId);
}
