package com.nkovljen.flightadvisor.service.impl;

import com.nkovljen.flightadvisor.dto.CustomUserPrincipal;
import com.nkovljen.flightadvisor.model.User;
import com.nkovljen.flightadvisor.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) {
        User user = userRepository.findByUsername(username);
        return new CustomUserPrincipal(user);
    }

}


