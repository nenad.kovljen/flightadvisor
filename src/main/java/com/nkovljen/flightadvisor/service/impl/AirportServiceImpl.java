package com.nkovljen.flightadvisor.service.impl;

import com.nkovljen.flightadvisor.exceptions.EntityNotFoundException;
import com.nkovljen.flightadvisor.model.Airport;
import com.nkovljen.flightadvisor.repository.AirportRepository;
import org.beanio.BeanReader;
import org.beanio.InvalidRecordException;
import org.beanio.MalformedRecordException;
import org.beanio.StreamFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.File;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class AirportServiceImpl implements com.nkovljen.flightadvisor.service.AirportService {

    private static final Logger LOG = LoggerFactory.getLogger("file");
    private static final String DELIMITER = "/";
    private Map<String, Airport> airportMap = new HashMap();
    public static final Map<String, Airport> airportCoordinatesMap = new ConcurrentHashMap<>();

    @Autowired
    private AirportRepository airportRepository;

    public AirportServiceImpl() throws URISyntaxException {
        cashAirports();
    }

    @Override
    @Transactional
    public List<Airport> importAirports(String cityName) {

        List<Airport> airports = new ArrayList();
        for(String key : airportMap.keySet()) {
            if (key.endsWith(DELIMITER + cityName)) {
                Airport airport = airportMap.get(key);
                airportRepository.save(airport);
                airports.add(airport);
            }
        }

        return airports;
    }

    @Override
    @Transactional
    public List<Airport> findAirportForCity(String cityName) {
        List<Airport> airports = airportRepository.findByCity(cityName);
        if (airports.isEmpty()) {
            throw new EntityNotFoundException(cityName);
        }
        return airports;
    }

    @Override
    @Transactional
    public void deleteAirport(Airport airport) {
        airportRepository.delete(airport);
    }

    @Override
    @Transactional
    public void deleteAirportsForCity(String cityName) {
        List<Airport> airports = findAirportForCity(cityName);
        for(Airport airport : airports) {
            airportRepository.delete(airport);
        }
    }

    private void cashAirports() throws URISyntaxException {
        StreamFactory factory = StreamFactory.newInstance();
        factory.loadResource("airport.xml");
        BeanReader in = factory.createReader("airport", new File(getClass().getClassLoader().getResource("data/airports.txt").toURI()));

        Object record = null;
        do {
            try {
                record = in.read();
                Airport airport = (Airport) record;
                if (airport != null && airport.getCity() != null) {
                    airportMap.put(createAirportKey(airport), airport);
                    airportCoordinatesMap.put(airport.getIata(), airport);
                }
            } catch (MalformedRecordException | InvalidRecordException ex) {
                LOG.error("Could not parse airports.txt correctly. Record: {} Error: {}", record, ex.getMessage());
            }
        }
        while (record != null);

        in.close();
    }

    private String createAirportKey(Airport airport) {
        return airport.getAirportId() + DELIMITER + airport.getCity();
    }

}
