package com.nkovljen.flightadvisor.service.impl;

import com.nkovljen.flightadvisor.dto.CustomUserPrincipal;
import com.nkovljen.flightadvisor.model.User;
import com.nkovljen.flightadvisor.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsPasswordService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

//@Transactional
//@Service
public class UserPasswordService implements UserDetailsPasswordService {

    @Autowired
    private UserRepository userRepository;

    // constructor ...

    @Override
    public UserDetails updatePassword(UserDetails userDetails, String newPassword) {
        User userCredentials = userRepository.findByUsername(userDetails.getUsername());
        userCredentials.setPassword(newPassword);
        return new CustomUserPrincipal(userCredentials);
    }

}