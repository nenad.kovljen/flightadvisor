package com.nkovljen.flightadvisor.service.impl;

import com.nkovljen.flightadvisor.dto.CityDto;
import com.nkovljen.flightadvisor.exceptions.EntityAlreadyExistsException;
import com.nkovljen.flightadvisor.exceptions.EntityNotFoundException;
import com.nkovljen.flightadvisor.exceptions.NotOwnerException;
import com.nkovljen.flightadvisor.model.Airport;
import com.nkovljen.flightadvisor.model.City;
import com.nkovljen.flightadvisor.model.Comment;
import com.nkovljen.flightadvisor.repository.CityRepository;
import com.nkovljen.flightadvisor.repository.UserRepository;
import com.nkovljen.flightadvisor.service.AirportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class CityServiceImpl implements com.nkovljen.flightadvisor.service.CityService {

    public static final String COMMENT_WITH_ID = "Comment with id:'";
    public static final String DOES_NOT_EXIST = "' does not exist";
    @Autowired
    private CityRepository cityRepository;

    @Autowired
    private RouteServiceImpl routeServiceImpl;

    @Autowired
    private AirportService airportService;

    @Autowired
    private UserRepository userRepository;

    @Override
    @Transactional
    public CityDto save(CityDto city) {
        City city1 = cityRepository.findByName(city.getName());
        if (city1 != null) {
            throw new EntityAlreadyExistsException(city.getName());
        }

        city1 = cityRepository.save(new City(city));
        List<Airport> airports = airportService.importAirports(city.getName());
        for(Airport airport: airports) {
            routeServiceImpl.importRoutes(airport.getAirportId());
        }

        CityDto cityDto = new CityDto(city1);
        filterNrOfComments(cityDto, 0);
        return cityDto;
    }

    @Override
    @Transactional
    public List<CityDto> findAll(Integer nrOfComments) {
        List<City> cities = cityRepository.findAll();
        List<CityDto> citiesDto = new ArrayList<>();
        for(City city : cities) {
            CityDto cityDto = new CityDto(city);
            filterNrOfComments(cityDto, nrOfComments);
            citiesDto.add(cityDto);
        }
        return citiesDto;
    }

    @Override
    @Transactional
    public CityDto getCity(Long cityId, Integer nrOfComments) {
        City cityData = findCity(cityId);
        CityDto cityDto = new CityDto(cityData);
        filterNrOfComments(cityDto, 0);
        return cityDto;
    }

    @Override
    @Transactional
    public CityDto addComment(Long cityId, String commentText) {
        City cityData = findCity(cityId);

        cityData.getComments().add(createComment(commentText));
        cityData = cityRepository.save(cityData);

        CityDto cityDto = new CityDto(cityData);
        filterNrOfComments(cityDto, 0);
        return cityDto;
    }

    @Override
    @Transactional
    public CityDto updateComment(Long cityId, String commentText, Long commentId) {
        City cityData = findCity(cityId);
        boolean commentFound = false;
        if (cityData.getComments().isEmpty()) {
            throw new EntityNotFoundException(COMMENT_WITH_ID + commentId + DOES_NOT_EXIST);
        }
        for(Comment comment : cityData.getComments()) {
            if (comment.getId().equals(commentId)) {
                if (!checkOwner(comment)) {
                    throw new NotOwnerException();
                }
                comment.setDescription(commentText);
                comment.setModifiedDate(new Date());
                commentFound = true;
                break;
            }
        }
        if (!commentFound) {
            throw new EntityNotFoundException(COMMENT_WITH_ID + commentId + DOES_NOT_EXIST);
        }
        cityData = cityRepository.save(cityData);

        CityDto cityDto = new CityDto(cityData);
        filterNrOfComments(cityDto, 0);
        return cityDto;
    }

    @Override
    @Transactional
    public CityDto removeComment(Long cityId, Long commentId) {
        City cityData = findCity(cityId);
        boolean commentFound = false;
        if (cityData.getComments().isEmpty()) {
            throw new EntityNotFoundException(COMMENT_WITH_ID + commentId + DOES_NOT_EXIST);
        }
        for(Comment comment : cityData.getComments()) {
            if (comment.getId().equals(commentId)) {
                if (!checkOwner(comment)) {
                    throw new NotOwnerException();
                }
                cityData.getComments().remove(comment);
                commentFound = true;
                break;
            }
        }
        if (!commentFound) {
            throw new EntityNotFoundException(COMMENT_WITH_ID + commentId + DOES_NOT_EXIST);
        }
        cityData = cityRepository.save(cityData);

        CityDto cityDto = new CityDto(cityData);
        filterNrOfComments(cityDto, 0);
        return cityDto;
    }

    @Override
    @Transactional
    public void delete(Long cityId) {
        City cityData = findCity(cityId);
        cityRepository.deleteById(cityId);
        List<Airport> airports = airportService.findAirportForCity(cityData.getName());

        for(Airport airport : airports){
            airportService.deleteAirport(airport);
            routeServiceImpl.deleteRoutesForAirport(airport.getAirportId());
        }
    }

    private Comment createComment(String commentText) {
        return Comment.builder().description(commentText).createDate(new Date()).modifiedDate(new Date()).user(userRepository.findByUsername(getCurrentLoggedInUser())).build();
    }

    private boolean checkOwner(Comment comment) {
        return comment.getUser().getId().equals(userRepository.findByUsername(getCurrentLoggedInUser()).getId());
    }

    private String getCurrentLoggedInUser() {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }

    private void filterNrOfComments(CityDto city, Integer nrOfComments) {
        if (nrOfComments > 0 && !city.getComments().isEmpty() && nrOfComments < city.getComments().size()) {
            city.setComments(city.getComments().subList(0, nrOfComments));
        }
    }

    private City findCity(Long cityId) {
        Optional<City> optional = cityRepository.findById(cityId);
        if (!optional.isPresent()) {
            throw new EntityNotFoundException("City with id:'" + cityId + DOES_NOT_EXIST);
        }
        return optional.get();
    }


}
