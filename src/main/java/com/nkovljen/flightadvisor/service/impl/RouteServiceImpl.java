package com.nkovljen.flightadvisor.service.impl;

import com.nkovljen.flightadvisor.model.Airport;
import com.nkovljen.flightadvisor.model.Route;
import com.nkovljen.flightadvisor.model.RouteSummary;
import com.nkovljen.flightadvisor.model.RouteSummaryDto;
import com.nkovljen.flightadvisor.repository.RouteRepository;
import com.nkovljen.flightadvisor.service.AirportService;
import org.beanio.BeanReader;
import org.beanio.StreamFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.File;
import java.net.URISyntaxException;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class RouteServiceImpl implements com.nkovljen.flightadvisor.service.RouteService {

    private static final Logger LOG = LoggerFactory.getLogger("file");
    private static final String DELIMITER = "/";
    private Map<String, Route> routeMap = new HashMap();

    @Autowired
    private RouteRepository routeRepository;

    @Autowired
    private AirportService airportService;

    public RouteServiceImpl() throws URISyntaxException {
        cashRoutes();
    }

    @Override
    @Transactional
    public List<Route> importRoutes(Integer airportId) {
        List<Route> routes = new ArrayList();
        for(String key : routeMap.keySet()) {
            if (key.startsWith(airportId + DELIMITER) || key.endsWith(DELIMITER + airportId)) {
                Route route = routeMap.get(key);
                routeRepository.save(route);
                routes.add(route);
            }
        }

        return routes;
    }

    @Override
    @Transactional
    public List<RouteSummaryDto> findRoutesByAirportId(String sourceCity, String destinationCity, int nrOfStops) {
        List<RouteSummary> routes = new ArrayList<>();

        List<Airport> sourceAirports = airportService.findAirportForCity(sourceCity.replace("$20", " "));
        List<Airport> destinationAirports = airportService.findAirportForCity(destinationCity.replace("$20", " "));

        List<String> sourceIatas = sourceAirports.stream().map(Airport::getIata).collect(Collectors.toList());
        List<String> destinationIatas = destinationAirports.stream().map(Airport::getIata).collect(Collectors.toList());

        routes.addAll(findShortestRoute(sourceIatas, destinationIatas, nrOfStops));

        routes.sort(Comparator.comparing(RouteSummary::getPrice));
        return routes.stream().map(RouteSummaryDto::new).collect(Collectors.toList());
    }

    private List<RouteSummary> findShortestRoute(List<String> sourceIatas, List<String> destinationIatas, int nrOfStops) {
        List<RouteSummary> routes = new ArrayList<>();
        switch (nrOfStops) {
            case 3: routes.addAll(routeRepository.findConnectedFlightThreeStops(sourceIatas, destinationIatas));
            case 2: routes.addAll(routeRepository.findConnectedFlightTwoStops(sourceIatas, destinationIatas));
            case 1: routes.addAll(routeRepository.findConnectedFlightOneStop(sourceIatas, destinationIatas));
            default: routes.addAll(routeRepository.findDirectFlight(sourceIatas, destinationIatas));
            break;
        }
        return routes;
    }

    @Override
    @Transactional
    public void deleteRoutesForAirport(Integer airportId) {
            routeRepository.deleteBySourceAirportId(airportId);
            routeRepository.deleteByDestinationAirportId(airportId);
    }

    private void cashRoutes() throws URISyntaxException {
        BeanReader in = null;
            StreamFactory factory = StreamFactory.newInstance();
            factory.loadResource("route.xml");
            in = factory.createReader("route", new File(getClass().getClassLoader().getResource("data/routes.txt").toURI()));

            Object record = null;

            do {
                try {
                    record = in.read();
                    Route route = (Route) record;
                    routeMap.put(createRouteKey(route), route);
                } catch (Exception ex) {
                    LOG.error("Could not parse routes.txt correctly. Record: {} Error: {}", record, ex.getMessage());
                }
            }
            while (record != null);

            in.close();
    }

    private String createRouteKey(Route route) {
        return route.getSourceAirportId() + DELIMITER + route.getDestinationAirportId();
    }

}
