package com.nkovljen.flightadvisor.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidRouteException extends RuntimeException {

    public InvalidRouteException() {
        super("Destination city can't be the same as source city");
    }
}
