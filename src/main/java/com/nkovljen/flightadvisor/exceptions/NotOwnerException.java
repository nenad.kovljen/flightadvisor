package com.nkovljen.flightadvisor.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class NotOwnerException extends RuntimeException {

    public NotOwnerException() {
        super("Currently logged in used doesn't have rights to delete / modify this item");
    }

}
