package com.nkovljen.flightadvisor.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class EntityNotFoundException extends RuntimeException {

    public EntityNotFoundException(String message) {
        super(message);
    }
    // "City with name:'" + name + "' does not exist"

    // "City with id:'" + id + "' does not exist"

    // "User with id:'" + id + "' doesn't exist"

    // "Comment with id:'" + id + "' does not exist"
}
