package com.nkovljen.flightadvisor.repository;

import com.nkovljen.flightadvisor.model.Airport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AirportRepository extends JpaRepository <Airport, Long> {

    Airport save(Airport airport);

    void delete(Airport airport);

    List<Airport> findByCity(String name);
}
