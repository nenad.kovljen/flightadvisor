package com.nkovljen.flightadvisor.repository;

import com.nkovljen.flightadvisor.model.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {

    Comment save(Comment comment);

    void delete(Comment comment);

}
