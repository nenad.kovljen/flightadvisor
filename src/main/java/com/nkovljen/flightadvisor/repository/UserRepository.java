package com.nkovljen.flightadvisor.repository;

import com.nkovljen.flightadvisor.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User,Long> {

    List<User> findByFirstName(String firstName);

    List<User> findByLastName(String lastName);

    User findByUsername(String lastName);

    User save(User user);

    List<User> findAll();

    void deleteById(Long id);

    Optional<User> findById(Long id);


}