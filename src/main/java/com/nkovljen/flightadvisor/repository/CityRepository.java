package com.nkovljen.flightadvisor.repository;

import com.nkovljen.flightadvisor.model.City;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CityRepository extends JpaRepository<City, Long> {

    City save(City city);

    List<City> findAll();

    City findByName(String name);

    Optional<City> findById(Long id);

    void deleteById(Long id);
}
