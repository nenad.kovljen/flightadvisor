package com.nkovljen.flightadvisor.repository;

import com.nkovljen.flightadvisor.model.Route;
import com.nkovljen.flightadvisor.model.RouteSummary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RouteRepository extends JpaRepository<Route, Long> {

    Route save(Route route);
    void delete(Route route);
    void deleteBySourceAirportId(Integer sourceAirportId);
    void deleteByDestinationAirportId(Integer destinationAirportId);

    @Query(nativeQuery = true, value = "SELECT distinct CONCAT(a1.source_airport, '-', a1.destination_airport) as route, a1.price as price FROM route a1 WHERE a1.source_airport in ?1 and a1.destination_airport in ?2 order by price asc")
    List<RouteSummary> findDirectFlight(List<String> sourceIatas, List<String> destinationIatas);

    @Query(nativeQuery = true, value = "SELECT distinct CONCAT(a1.source_airport, '-', a1.destination_airport, '-', a2.destination_airport) as route, a1.price + a2.price as price FROM route a1,route a2 WHERE (a1.source_airport in ?1 and a2.destination_airport in ?2) and (a2.source_airport = a1.destination_airport) order by price asc")
    List<RouteSummary> findConnectedFlightOneStop(List<String> sourceIatas, List<String> destinationIatas);

    @Query(nativeQuery = true, value = "SELECT distinct CONCAT(a1.source_airport, '-', a1.destination_airport, '-', a2.destination_airport, '-', a3.destination_airport) as route, a1.price + a2.price + a3.price as price FROM route a1, route a2, route a3 WHERE (a1.source_airport in ?1 and a3.destination_airport in ?2) and (a2.source_airport = a1.destination_airport and a3.source_airport = a2.destination_airport) order by price asc")
    List<RouteSummary> findConnectedFlightTwoStops(List<String> sourceIatas, List<String> destinationIatas);

    @Query(nativeQuery = true, value = "SELECT distinct CONCAT(a1.source_airport, '-', a1.destination_airport, '-', a2.destination_airport, '-', a3.destination_airport, '-', a4.destination_airport) as route, a1.price + a2.price + a3.price + a4.price as price FROM route a1, route a2, route a3, route a4 WHERE (a1.source_airport in ?1 and a4.destination_airport in ?2) and (a2.source_airport = a1.destination_airport and a3.source_airport = a2.destination_airport and a4.source_airport = a3.destination_airport) order by price asc")
    List<RouteSummary> findConnectedFlightThreeStops(List<String> sourceIatas, List<String> destinationIatas);

    @Query(nativeQuery = true, value = "SELECT distinct CONCAT(a1.source_airport, '-', a1.destination_airport, '-', a2.destination_airport, '-', a3.destination_airport, '-', a4.destination_airport, '-', a5.destination_airport) as route, a1.price + a2.price + a3.price + a4.price + a5.price as price FROM route a1, route a2, route a3, route a4, route a5 WHERE (a1.source_airport in ?1 and a5.destination_airport in ?2) and (a2.source_airport = a1.destination_airport and a3.source_airport = a2.destination_airport and a4.source_airport = a3.destination_airport and a5.source_airport = a4.destination_airport) order by price asc")
    List<RouteSummary> findConnectedFlightFourStops(List<String> sourceIatas, List<String> destinationIatas);

}
