package com.nkovljen.flightadvisor;

import com.nkovljen.flightadvisor.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private static final String ADMIN_ROLE = "ADMIN";
    public static final String USER_ROLE = "USER";

    @Autowired
    private UserServiceImpl userService;

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                .csrf()
                .disable()
                .authorizeRequests().antMatchers(HttpMethod.POST, "/flightadvisor/users/register/").permitAll()
                .and()
                .httpBasic().and().authorizeRequests()
                    .antMatchers(HttpMethod.DELETE, "/flightadvisor/users/**").hasAuthority(ADMIN_ROLE)
                    .antMatchers(HttpMethod.GET, "/flightadvisor/users/**").hasAuthority(ADMIN_ROLE)
                    .antMatchers(HttpMethod.GET, "/flightadvisor/routes/**").hasAuthority(USER_ROLE)
                    .antMatchers(HttpMethod.POST, "/flightadvisor/cities/").hasAuthority(ADMIN_ROLE)
                    .antMatchers(HttpMethod.DELETE, "/flightadvisor/cities").hasAuthority(ADMIN_ROLE)
                    .antMatchers(HttpMethod.POST,"/flightadvisor/cities/*/comment/**").hasAuthority(USER_ROLE)
                    .antMatchers(HttpMethod.DELETE,"/flightadvisor/cities/*/comment/**").hasAuthority(USER_ROLE)
                    .antMatchers(HttpMethod.GET, "/flightadvisor/cities/**").hasAuthority(USER_ROLE);

    }

    @Bean
    public AuthenticationProvider daoAuthenticationProvider() {
        DaoAuthenticationProvider provider =
                new DaoAuthenticationProvider();
        provider.setPasswordEncoder(passwordEncoder());
        provider.setUserDetailsService(this.userService);
        return provider;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(11);
    }

}