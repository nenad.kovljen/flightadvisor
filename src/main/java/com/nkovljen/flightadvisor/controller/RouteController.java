package com.nkovljen.flightadvisor.controller;

import com.nkovljen.flightadvisor.exceptions.InvalidRouteException;
import com.nkovljen.flightadvisor.model.RouteSummaryDto;
import com.nkovljen.flightadvisor.service.impl.RouteServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@RequestMapping(value = "/flightadvisor/routes", consumes = { "application/json"}, produces = { "application/json"})
@Controller
public class RouteController {

    @Autowired
    private RouteServiceImpl routeServiceImpl;

    @GetMapping("/{sourceCity}/{destinationCity}")
    public ResponseEntity getAllRoutesForDestinations(@PathVariable("sourceCity") String sourceCity, @PathVariable("destinationCity") String destinationCity, @RequestParam(name = "nrOfStops", defaultValue = "3") Integer nrOfStops) {
        if (sourceCity.equals(destinationCity)) {
            throw new InvalidRouteException();
        }
        List<RouteSummaryDto> routes = routeServiceImpl.findRoutesByAirportId(sourceCity, destinationCity, nrOfStops);
        return ResponseEntity.ok(routes);
    }

}
