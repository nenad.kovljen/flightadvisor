package com.nkovljen.flightadvisor.controller;

import com.nkovljen.flightadvisor.dto.UserDto;
import com.nkovljen.flightadvisor.exceptions.EntityNotFoundException;
import com.nkovljen.flightadvisor.model.User;
import com.nkovljen.flightadvisor.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.nkovljen.flightadvisor.SecurityConfig.USER_ROLE;

@Controller
@RequestMapping(value= "/flightadvisor/users", consumes = { "application/json"}, produces = { "application/json"})
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostMapping("/registration")
    @ResponseStatus(code = HttpStatus.CREATED)
    public void register(@RequestBody UserDto userDto) {
        User user = User.builder()
                .firstName(userDto.getFirstName())
                .lastName(userDto.getLastName())
                .username(userDto.getUsername())
                .password(passwordEncoder.encode(userDto.getPassword()))
                .roles(getRoles(userDto))
                .build();
        userRepository.save(user);
    }

    @GetMapping
    public ResponseEntity getAllUsers() {
        List<User> users = userRepository.findAll();
        return ResponseEntity.ok(users.stream().map(UserDto::new).collect(Collectors.toList()));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteUser(@PathVariable("id") Long id) {
        Optional<User> userOptional = userRepository.findById(id);
        if (!userOptional.isPresent()) {
            throw new EntityNotFoundException("User with id:'" + id + "' doesn't exist");
        }
        userRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }

    private String[] getRoles(UserDto userDto) {
        return userDto.getRoles() != null ? userDto.getRoles() : new String[] {USER_ROLE};
    }

}
