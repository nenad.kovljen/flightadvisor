package com.nkovljen.flightadvisor.controller;

import com.nkovljen.flightadvisor.dto.CityDto;
import com.nkovljen.flightadvisor.service.impl.CityServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping(value = "/flightadvisor/cities", consumes = { "application/json"}, produces = { "application/json"})
@Controller
public class CityController {

    @Autowired
    private CityServiceImpl cityServiceImpl;

    @GetMapping
    public ResponseEntity getAll(@RequestParam(name = "nrOfComments", defaultValue = "0") Integer nrOfComments) {
        List<CityDto> cities = cityServiceImpl.findAll(nrOfComments);
        return ResponseEntity.ok(cities);
    }

    @GetMapping(path = "/{cityId}")
    public ResponseEntity getCity(@PathVariable("cityId") Long cityId, @RequestParam(name = "nrOfComments", defaultValue = "0") Integer nrOfComments) {
        CityDto city = cityServiceImpl.getCity(cityId, nrOfComments);
        return ResponseEntity.ok(city);
    }

    @PostMapping
    public ResponseEntity addCity(@RequestBody CityDto city) {
        CityDto city1 = cityServiceImpl.save(city);
        if (city1.getId() != null) {
            return ResponseEntity.ok(city1);
        }
        return ResponseEntity.badRequest().build();
    }

    @PostMapping("/{cityId}/comment")
    public ResponseEntity addComment(@PathVariable("cityId") Long  cityId, @RequestBody String comment) {
        CityDto city1 = cityServiceImpl.addComment(cityId, comment);
        if (city1.getId() != null) {
            return ResponseEntity.ok(city1);
        }
        return ResponseEntity.badRequest().build();
    }

    @PostMapping("/{cityId}/comment/{commentId}")
    public ResponseEntity addComment(@PathVariable("cityId") Long cityId, @RequestBody String comment, @PathVariable("commentId") Long commentId) {
        CityDto city1 = cityServiceImpl.updateComment(cityId, comment, commentId);
        if (city1.getId() != null) {
            return ResponseEntity.ok(city1);
        }
        return ResponseEntity.badRequest().build();
    }

    @DeleteMapping("/{cityId}/comment/{commentId}")
    public ResponseEntity addComment(@PathVariable("cityId") Long cityId, @PathVariable("commentId") Long commentId) {
        CityDto city1 = cityServiceImpl.removeComment(cityId, commentId);
        if (city1.getId() != null) {
            return ResponseEntity.ok(city1);
        }
        return ResponseEntity.badRequest().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity removeCity(@PathVariable("id") Long cityId) {
        cityServiceImpl.delete(cityId);
        return ResponseEntity.ok().build();
    }

}
