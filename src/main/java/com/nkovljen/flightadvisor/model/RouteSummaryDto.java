package com.nkovljen.flightadvisor.model;

import lombok.Data;

import static com.nkovljen.flightadvisor.service.impl.AirportServiceImpl.airportCoordinatesMap;

@Data
public class RouteSummaryDto {

    private String route;
    private String price;
    private String length;

    private static final Double EARTH_RADIUS = 6371e3;

    public RouteSummaryDto(RouteSummary routeSummary) {
        this.route = routeSummary.getRoute();
        this.price = String.format("%.3f", routeSummary.getPrice());
        this.length = calculateLength(route);
    }

    private String calculateLength(String route) {
        String[] stops = route.split("-");
        Double sum = 0d;
        for(int i = 0; i < stops.length -1 ; i++) {

            Airport airportA = airportCoordinatesMap.get(stops[i]);
            Airport airportB = airportCoordinatesMap.get(stops[i + 1]);

            if (airportA == null || airportA.getLatitude() == null || airportA.getLongitude() == null ) {
                return "N/A";
            }
            if (airportB == null || airportB.getLatitude() == null || airportB.getLongitude() == null ) {
                return "N/A";
            }

            double latA = Double.parseDouble(airportA.getLatitude());
            double lonA = Double.parseDouble(airportA.getLongitude());

            double latB = Double.parseDouble(airportB.getLatitude());
            double lonB = Double.parseDouble(airportB.getLongitude());

            sum += calculateDistanceFromAToB(latA, lonA, latB, lonB);

        }
        return String.valueOf(String.format("%.3f km", sum));
    }

    private Double calculateDistanceFromAToB(double latA, double lonA, double latB, double lonB) {

        Double fiA = Math.toRadians(latA);
        Double fiB = Math.toRadians(latB);

        Double deltaFi = Math.toRadians(latB - latA);
        Double deltaLambda = Math.toRadians(lonB - lonA);

        Double a = Math.sin(deltaFi/2) * Math.sin(deltaFi/2) +
                Math.cos(fiA) * Math.cos(fiB) *
                        Math.sin(deltaLambda/2) * Math.sin(deltaLambda/2);

        Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

        return EARTH_RADIUS * c /1000;

    }

}
