package com.nkovljen.flightadvisor.model;

import com.nkovljen.flightadvisor.dto.CommentDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String description;
    private Date createDate;
    private Date modifiedDate;

    @ManyToOne
    @JoinColumn
    private User user;

    public Comment(CommentDto commentDto) {
        this.id = commentDto.getId();
        this.description = commentDto.getDescription();
        this.createDate = commentDto.getCreateDate();
        this.modifiedDate = commentDto.getModifiedDate();
    }



}
