package com.nkovljen.flightadvisor.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
public class Route {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String airline;
    private String airlineId;
    private String sourceAirport;
    private Integer sourceAirportId;
    private String destinationAirport;
    private Integer destinationAirportId;
    private char codeshare;
    private Integer stops;
    private String equipment;
    private Double price;

}
