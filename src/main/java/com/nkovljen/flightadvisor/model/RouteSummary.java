package com.nkovljen.flightadvisor.model;

public interface RouteSummary {

    String getRoute();
    Double getPrice();

}
