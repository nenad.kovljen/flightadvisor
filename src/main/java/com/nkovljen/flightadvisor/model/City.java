package com.nkovljen.flightadvisor.model;

import com.nkovljen.flightadvisor.dto.CityDto;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Data
@NoArgsConstructor
public class City {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(unique = true)
    private String name;
    private String country;
    private String description;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Comment> comments;

    public City(CityDto cityDto) {
        this.name = cityDto.getName();
        this.country = cityDto.getCountry();
        this.description = cityDto.getDescription();
        this.comments = cityDto.getComments().stream().map(Comment::new).collect(Collectors.toList());
    }
    
}
