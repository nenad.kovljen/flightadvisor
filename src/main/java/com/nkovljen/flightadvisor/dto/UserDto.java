package com.nkovljen.flightadvisor.dto;

import com.nkovljen.flightadvisor.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Arrays;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {

    private Long id;
    private String firstName;
    private String lastName;
    private String username;
    private String password;
    private String[] roles;

    public UserDto(User user) {
        this.id = user.getId();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.username = user.getUsername();
        this.password = user.getPassword();
        if (user.getRoles() != null) {
            this.roles = Arrays.copyOf(user.getRoles(), user.getRoles().length);
        }

    }

}
