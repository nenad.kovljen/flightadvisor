package com.nkovljen.flightadvisor.dto;

import com.nkovljen.flightadvisor.model.Comment;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommentDto {
    private Long id;

    private String description;
    private Date createDate;
    private Date modifiedDate;

    public CommentDto(Comment comment) {
        this.id = comment.getId();
        this.description = comment.getDescription();
        this.createDate = comment.getCreateDate();
        this.modifiedDate = comment.getModifiedDate();
    }
}
