package com.nkovljen.flightadvisor.dto;

import com.nkovljen.flightadvisor.model.City;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CityDto {

    private Long id;
    private String name;
    private String country;
    private String description;
    private List<CommentDto> comments = new ArrayList<>();

    public CityDto(City city) {
        this.id = city.getId();
        this.name = city.getName();
        this.country = city.getCountry();
        this.description = city.getDescription();
        List<CommentDto> commentList  = new ArrayList<>();
        if (city.getComments() != null) {
            commentList = city.getComments().stream().map(CommentDto::new).collect(Collectors.toList());
            commentList.sort((c1, c2) -> c2.getCreateDate().compareTo(c1.getCreateDate()));
        }
        this.comments = commentList;
    }

}
